--
-- PostgreSQL database dump
--

-- Dumped from database version 13.0
-- Dumped by pg_dump version 13.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


--
-- Name: a_a_sp_most_expensive_movie(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.a_a_sp_most_expensive_movie() RETURNS double precision
    LANGUAGE plpgsql
    AS $$
    DECLARE
        res1 double precision;
    BEGIN
        WITH max_price_movie AS (
                    SELECT *
                    from movies
                    where price = (select max(price) from movies)
                    )
        select price into res1
        from movies
        where movies.price = (select price from max_price_movie);

        return res1;
    END;
    $$;


ALTER FUNCTION public.a_a_sp_most_expensive_movie() OWNER TO postgres;

--
-- Name: a_sp_count_sum_records(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.a_sp_count_sum_records() RETURNS bigint
    LANGUAGE plpgsql
    AS $$
    DECLARE
        MOVIES_COUNT bigint :=0;
        COUNTRY_COUNT bigint :=0;
    BEGIN
        SELECT COUNT(*) INTO MOVIES_COUNT
        FROM movies;

        SELECT COUNT(*) INTO COUNTRY_COUNT
        FROM country;

        return COUNTRY_COUNT + MOVIES_COUNT;
    END;
    $$;


ALTER FUNCTION public.a_sp_count_sum_records() OWNER TO postgres;

--
-- Name: a_sp_div(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.a_sp_div(x integer, y integer) RETURNS double precision
    LANGUAGE plpgsql
    AS $$
DECLARE
  result double precision := 0;
BEGIN
  if y = 0 THEN
      RAISE division_by_zero; -- this is like throw new error
  end if;
  result := x::double precision / y::double precision;
  return result;
EXCEPTION -- if we not catch the exception here -- it will be promoted to calling block
          -- raising errors and not catching them is like the SQL itself raised the exception
 WHEN division_by_zero THEN
 RAISE NOTICE 'caught division_by_zero'; -- this is message to console
 RETURN null;

END;
$$;


ALTER FUNCTION public.a_sp_div(x integer, y integer) OWNER TO postgres;

--
-- Name: a_sp_get_movie_price(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.a_sp_get_movie_price(OUT min_price double precision, OUT max_price double precision, OUT avg_price double precision) RETURNS record
    LANGUAGE plpgsql
    AS $$
begin
  select min(price),
         max(price),
		 avg(price)::numeric(5,2)
  into min_price, max_price, avg_price
  from movies;
end;$$;


ALTER FUNCTION public.a_sp_get_movie_price(OUT min_price double precision, OUT max_price double precision, OUT avg_price double precision) OWNER TO postgres;

--
-- Name: a_sp_get_movies_in_range(double precision, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.a_sp_get_movies_in_range(min_price double precision, max_price double precision) RETURNS TABLE(id bigint, title text, release_date timestamp without time zone, price double precision, country_name text)
    LANGUAGE plpgsql
    AS $$
    BEGIN
        RETURN QUERY
        SELECT m.id, m.title, m.release_date, m.price, c.name FROM movies m
        join country c on m.country_id = c.id
        WHERE m.price between min_price and max_price;
    END;
$$;


ALTER FUNCTION public.a_sp_get_movies_in_range(min_price double precision, max_price double precision) OWNER TO postgres;

--
-- Name: a_sp_get_movies_mid(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.a_sp_get_movies_mid() RETURNS TABLE(id bigint, title text, release_date timestamp without time zone, price double precision, country_name text)
    LANGUAGE plpgsql
    AS $$
    BEGIN
        RETURN QUERY
        WITH cheapest_movie AS
            (
                select * from movies
                where movies.price = (select min(movies.price) from movies)
            ),
        expansive_movie AS
            (
                select * from movies
                where movies.price = (select max(movies.price) from movies)
            )
        SELECT m.id, m.title, m.release_date, m.price, c.name FROM movies m
        join country c on m.country_id = c.id
        WHERE m.id <> (select cheapest_movie.id from cheapest_movie) and m.id <> (select expansive_movie.id from expansive_movie);
    END;
$$;


ALTER FUNCTION public.a_sp_get_movies_mid() OWNER TO postgres;

--
-- Name: a_sp_get_randoms(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.a_sp_get_randoms(_max integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
    BEGIN
        return (random() * _max);
    END;
$$;


ALTER FUNCTION public.a_sp_get_randoms(_max integer) OWNER TO postgres;

--
-- Name: a_sp_insert_movie(text, timestamp without time zone, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.a_sp_insert_movie(_title text, _release_date timestamp without time zone, _price double precision) RETURNS bigint
    LANGUAGE plpgsql
    AS $$
    DECLARE
        new_id bigint;
    BEGIN
        INSERT INTO movies (title, release_date, price)
        VALUES (_title, _release_date, _price)
        returning id into new_id;

        return new_id;
    END;
    $$;


ALTER FUNCTION public.a_sp_insert_movie(_title text, _release_date timestamp without time zone, _price double precision) OWNER TO postgres;

--
-- Name: a_sp_loop1(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.a_sp_loop1() RETURNS integer
    LANGUAGE plpgsql
    AS $$
    declare
        sum int := 0;
    BEGIN
        FOR i IN 1..(select count(*) from country)
            loop
                sum := sum + (select id from country where id=i);
            end loop;
        return sum;
    END;
$$;


ALTER FUNCTION public.a_sp_loop1() OWNER TO postgres;

--
-- Name: a_sp_max(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.a_sp_max(x integer, y integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
    BEGIN
        IF x > y THEN
            RETURN x;
        else
            RETURN y;
        END IF;
END;
$$;


ALTER FUNCTION public.a_sp_max(x integer, y integer) OWNER TO postgres;

--
-- Name: a_sp_max(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.a_sp_max(x integer, y integer, z integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
    BEGIN
        IF x > y AND x > z THEN
            RETURN x;
        ELSEIF y > z THEN
            RETURN y;
        ELSE
            RETURN z;
        END IF;
END;
$$;


ALTER FUNCTION public.a_sp_max(x integer, y integer, z integer) OWNER TO postgres;

--
-- Name: a_sp_max_case(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.a_sp_max_case(_id_type text) RETURNS TABLE(ids integer)
    LANGUAGE plpgsql
    AS $$
    BEGIN
        return query
        select * from movies;

    END;
$$;


ALTER FUNCTION public.a_sp_max_case(_id_type text) OWNER TO postgres;

--
-- Name: a_sp_max_case(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.a_sp_max_case(x integer, y integer, z integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
    BEGIN
        CASE
            WHEN x > y AND x > z THEN
                RETURN x;
            WHEN y > z THEN
                RETURN y;
        ELSE
            RETURN z;
        END CASE;
END;
$$;


ALTER FUNCTION public.a_sp_max_case(x integer, y integer, z integer) OWNER TO postgres;

--
-- Name: a_sp_most_expensive_movie(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.a_sp_most_expensive_movie(OUT movie_name text) RETURNS text
    LANGUAGE plpgsql
    AS $$
    DECLARE
        max_price double precision :=0;
    BEGIN
        SELECT max(price)
        into max_price
        from movies;

        SELECT movies.title
        into movie_name
        from movies where movies.price = max_price;
    END;
    $$;


ALTER FUNCTION public.a_sp_most_expensive_movie(OUT movie_name text) OWNER TO postgres;

--
-- Name: a_sp_movie_id_1_or_2(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.a_sp_movie_id_1_or_2(_id_type text) RETURNS TABLE(ids bigint, title text)
    LANGUAGE plpgsql
    AS $$
    BEGIN
        return query
        SELECT CASE WHEN _id_type ='M' THEN movies.id else movies.country_id END ,
               movies.title from movies;
    END;
$$;


ALTER FUNCTION public.a_sp_movie_id_1_or_2(_id_type text) OWNER TO postgres;

--
-- Name: a_sp_populate_grade(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.a_sp_populate_grade(_classes integer, _students integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
    declare
        counter int := 0;
        _grade double precision := 0;
    BEGIN
        FOR i IN 1.._classes
            loop
                FOR j IN 1.._students -- for (int j = 1; j <= _students; j++)
                loop
                    counter := counter + 1;
                    _grade = random() * 100;
                    INSERT INTO grades(class_id, student_id, grade) VALUES
                        (i, counter, _grade);
                    end loop;
            end loop;
        return counter;
    END;
$$;


ALTER FUNCTION public.a_sp_populate_grade(_classes integer, _students integer) OWNER TO postgres;

--
-- Name: a_sp_return_price_or_sqr(boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.a_sp_return_price_or_sqr(_pow boolean) RETURNS TABLE(_title text, _price double precision)
    LANGUAGE plpgsql
    AS $$
    BEGIN
        return query
        SELECT movies.title,
               CASE WHEN NOT _pow THEN movies.price else pow(movies.price, 2) END
                from movies;
    END;
$$;


ALTER FUNCTION public.a_sp_return_price_or_sqr(_pow boolean) OWNER TO postgres;

--
-- Name: a_sp_return_price_or_sqr(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.a_sp_return_price_or_sqr(_choice integer) RETURNS TABLE(_title text, _price double precision)
    LANGUAGE plpgsql
    AS $$
    BEGIN
        return query
        SELECT movies.title,
               CASE WHEN _choice = 1 THEN movies.price else pow(movies.price, 2) END
                from movies;
    END;
$$;


ALTER FUNCTION public.a_sp_return_price_or_sqr(_choice integer) OWNER TO postgres;

--
-- Name: a_sp_sum_of_numbers(double precision, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.a_sp_sum_of_numbers(m double precision, n double precision) RETURNS double precision
    LANGUAGE plpgsql
    AS $$
        DECLARE
            x integer := 1;
        BEGIN
            RETURN n + m + x;
        END;
    $$;


ALTER FUNCTION public.a_sp_sum_of_numbers(m double precision, n double precision) OWNER TO postgres;

--
-- Name: a_sp_update_movie(bigint, text, timestamp without time zone, double precision, bigint); Type: PROCEDURE; Schema: public; Owner: postgres
--

CREATE PROCEDURE public.a_sp_update_movie(_id bigint, _title text, _release_date timestamp without time zone, _price double precision, _country_id bigint)
    LANGUAGE plpgsql
    AS $$
    BEGIN
        UPDATE movies
        SET title = _title, release_date = _release_date, price = _price, country_id = _country_id
        where id = _id;
    END;
    $$;


ALTER PROCEDURE public.a_sp_update_movie(_id bigint, _title text, _release_date timestamp without time zone, _price double precision, _country_id bigint) OWNER TO postgres;

--
-- Name: a_sp_upsert_movie(text, timestamp without time zone, double precision, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.a_sp_upsert_movie(_title text, _release_date timestamp without time zone, _price double precision, _country_id bigint) RETURNS bigint
    LANGUAGE plpgsql
    AS $$
    DECLARE
        record_id bigint;
    BEGIN
        SELECT movies.id
        into record_id
            FROM movies
            WHERE movies.title = _title;
        If not found THEN
            INSERT INTO movies (title, release_date, price, country_id)
            VALUES (_title, _release_date, _price, _country_id)
            returning id into record_id;
        ELSE
            UPDATE movies
                SET release_date = _release_date, price = _price, country_id = _country_id
            WHERE
                movies.id = record_id;
            end if;
            return record_id;
    END;
$$;


ALTER FUNCTION public.a_sp_upsert_movie(_title text, _release_date timestamp without time zone, _price double precision, _country_id bigint) OWNER TO postgres;

--
-- Name: a_sum_n_product(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.a_sum_n_product(x integer, y integer, OUT prod integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
    DECLARE
        sum integer := 0;
BEGIN
 sum := x + y;
 prod := x * y;
END;
$$;


ALTER FUNCTION public.a_sum_n_product(x integer, y integer, OUT prod integer) OWNER TO postgres;

--
-- Name: hello_world(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.hello_world() RETURNS character varying
    LANGUAGE plpgsql
    AS $$
        BEGIN
            RETURN CONCAT('Hello ','World! ', ' ', current_timestamp);
        END;
    $$;


ALTER FUNCTION public.hello_world() OWNER TO postgres;

--
-- Name: sp_get_all_movies(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.sp_get_all_movies() RETURNS TABLE(id bigint, title text, release_date timestamp without time zone, price double precision, name text)
    LANGUAGE plpgsql
    AS $$
    BEGIN
        return query
            SELECT movies.id, movies.title, movies.release_date, movies.price, c.name FROM movies
            join country c on movies.country_id = c.id;
    END;
    $$;


ALTER FUNCTION public.sp_get_all_movies() OWNER TO postgres;

--
-- Name: sp_get_all_movies_by_country(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.sp_get_all_movies_by_country(_country_name text) RETURNS TABLE(id bigint, title text, release_date timestamp without time zone, price double precision, name text)
    LANGUAGE plpgsql
    AS $$
    BEGIN
        return query
            SELECT movies.id, movies.title, movies.release_date, movies.price, c.name FROM movies
            join country c on movies.country_id = c.id
            where c.name = _country_name;
    END;
    $$;


ALTER FUNCTION public.sp_get_all_movies_by_country(_country_name text) OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: Employee; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Employee" (
    "Id" bigint NOT NULL,
    "Name" character varying,
    "Salary" double precision
);


ALTER TABLE public."Employee" OWNER TO postgres;

--
-- Name: movies; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.movies (
    id bigint NOT NULL,
    title text NOT NULL,
    release_date timestamp without time zone NOT NULL,
    price double precision DEFAULT 0,
    country_id bigint
);


ALTER TABLE public.movies OWNER TO postgres;

--
-- Name: Movies_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Movies_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Movies_id_seq" OWNER TO postgres;

--
-- Name: Movies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Movies_id_seq" OWNED BY public.movies.id;


--
-- Name: country; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.country (
    id bigint NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.country OWNER TO postgres;

--
-- Name: all_movies; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.all_movies AS
 SELECT movies.id,
    movies.title,
    movies.release_date,
    movies.price,
    c.name
   FROM (public.movies
     JOIN public.country c ON ((movies.country_id = c.id)));


ALTER TABLE public.all_movies OWNER TO postgres;

--
-- Name: country_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.country_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.country_id_seq OWNER TO postgres;

--
-- Name: country_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.country_id_seq OWNED BY public.country.id;


--
-- Name: grades; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.grades (
    id bigint NOT NULL,
    class_id bigint NOT NULL,
    student_id bigint NOT NULL,
    grade double precision DEFAULT 0 NOT NULL
);


ALTER TABLE public.grades OWNER TO postgres;

--
-- Name: grades_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.grades_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grades_id_seq OWNER TO postgres;

--
-- Name: grades_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.grades_id_seq OWNED BY public.grades.id;


--
-- Name: student; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.student (
    admission integer NOT NULL,
    name text NOT NULL,
    age integer NOT NULL,
    course character(50),
    department character(50)
);


ALTER TABLE public.student OWNER TO postgres;

--
-- Name: country id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.country ALTER COLUMN id SET DEFAULT nextval('public.country_id_seq'::regclass);


--
-- Name: grades id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grades ALTER COLUMN id SET DEFAULT nextval('public.grades_id_seq'::regclass);


--
-- Name: movies id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movies ALTER COLUMN id SET DEFAULT nextval('public."Movies_id_seq"'::regclass);


--
-- Data for Name: Employee; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Employee" ("Id", "Name", "Salary") FROM stdin;
\.


--
-- Data for Name: country; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.country (id, name) FROM stdin;
1	ISRAEl
2	USA
\.


--
-- Data for Name: grades; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.grades (id, class_id, student_id, grade) FROM stdin;
1	1	1	92.97656181981218
3	1	3	63.06460937032021
4	1	4	35.76203307639432
5	1	5	97.85863094275591
6	1	6	70.75336447710185
7	1	7	94.6042050824996
8	1	8	99.39057098853894
9	1	9	31.777077805061893
10	1	10	17.668886860427335
11	1	11	6.0190478961285265
12	1	12	72.70179277093902
13	1	13	12.87263332809836
14	1	14	26.772073239567362
15	1	15	94.57796000558965
16	1	16	6.811687163142466
17	1	17	31.299579086588736
18	1	18	10.877595906164572
19	1	19	21.89223526561328
20	1	20	50.79783302363623
21	1	21	82.79712070050813
22	1	22	67.56425896819849
23	1	23	6.430416095413349
24	1	24	92.17790834245605
25	1	25	25.06202485736999
26	1	26	44.04995445488993
27	1	27	28.275645372618996
28	1	28	61.75364731144768
29	1	29	82.45860414001278
30	1	30	20.360692062905628
31	2	31	49.789919984787545
32	2	32	51.5360514595649
33	2	33	14.496619308629377
34	2	34	88.41671222556471
35	2	35	24.653359937729746
36	2	36	61.07265658812864
37	2	37	25.60064048753965
38	2	38	6.972306078350243
39	2	39	45.51645860444857
40	2	40	53.27867835913551
41	2	41	50.349148087089546
42	2	42	18.767245801255683
43	2	43	65.38372708618816
44	2	44	13.584812148493342
45	2	45	54.753942499067776
46	2	46	90.9368309913475
47	2	47	63.29521999790977
48	2	48	52.67175342547503
49	2	49	63.26854989016404
50	2	50	48.40713740740199
51	2	51	24.657662534457003
52	2	52	24.144039709721454
53	2	53	48.859041032250516
54	2	54	4.957269882575588
55	2	55	79.78132067074277
56	2	56	84.14500033778793
57	2	57	13.255302300651195
58	2	58	1.7089276707949352
59	2	59	20.19689904948585
60	2	60	54.13434209251804
61	3	61	72.85109399207137
62	3	62	58.41554422790409
63	3	63	65.86564576334517
64	3	64	53.906603460442426
65	3	65	46.875482212492514
66	3	66	51.04132067958318
67	3	67	32.475021094745316
68	3	68	6.55128413493955
69	3	69	95.4672180120955
70	3	70	98.27978924465413
71	3	71	86.96377958625696
72	3	72	26.635305626059136
73	3	73	61.01064244915797
74	3	74	69.95977591461582
75	3	75	41.88865282709138
76	3	76	47.67948972169833
77	3	77	44.21250009787947
78	3	78	98.3839369881025
79	3	79	31.18703530708835
80	3	80	24.319360319234562
81	3	81	72.40192742622078
82	3	82	57.76407787243585
83	3	83	7.575698858522273
84	3	84	21.765687061758143
85	3	85	49.72162770910167
86	3	86	81.84338114354262
87	3	87	76.83662382812138
88	3	88	32.75320251871179
89	3	89	83.36013195968413
90	3	90	71.87613045023653
91	4	91	28.472068446890475
92	4	92	6.5907445970331935
93	4	93	55.678857006975946
94	4	94	39.2806737092144
95	4	95	72.86903314364785
96	4	96	41.769108705810964
97	4	97	15.751688571622324
98	4	98	63.96387941309314
99	4	99	59.717845029632244
100	4	100	52.47304617500319
101	4	101	35.00982024351096
102	4	102	91.57036001541599
103	4	103	33.81272071610617
104	4	104	28.972522796932054
105	4	105	57.73384989469896
106	4	106	53.3349225137826
107	4	107	5.668394264577259
108	4	108	44.98946036805691
109	4	109	58.2344545191777
110	4	110	59.97221477510948
111	4	111	44.073252660079376
112	4	112	33.566131007942346
113	4	113	30.700614027124473
114	4	114	86.84602308550353
115	4	115	74.53543499190083
116	4	116	32.57906507237678
117	4	117	5.671115385547765
118	4	118	48.80729811133335
119	4	119	25.64605477321713
120	4	120	56.78917490826123
121	5	121	37.514221939615666
122	5	122	28.422399177809154
123	5	123	59.07762878128118
124	5	124	64.19869476917697
125	5	125	1.6077200194647645
126	5	126	16.241406756734023
127	5	127	47.962971703143964
128	5	128	68.56489817245475
129	5	129	97.33533728591972
130	5	130	93.25331655606561
131	5	131	2.779774978324312
132	5	132	89.32828408110751
133	5	133	75.60654594531151
134	5	134	7.275651870529032
135	5	135	48.93086178941637
136	5	136	96.14042878715239
137	5	137	7.228300218465122
138	5	138	91.82817013474818
139	5	139	21.604229414318965
140	5	140	82.87788750663694
141	5	141	23.785092531935348
142	5	142	49.70405116821439
143	5	143	92.17748636959122
144	5	144	19.819804721098677
145	5	145	96.20612806070206
146	5	146	77.19989466488784
147	5	147	77.66769619338412
148	5	148	70.92716146522591
149	5	149	51.216193740794225
150	5	150	68.58317930932856
\.


--
-- Data for Name: movies; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.movies (id, title, release_date, price, country_id) FROM stdin;
5	Superman returns	2021-05-09 22:00:00	75.4	2
1	batman returns	2020-12-16 20:21:30.5	19.5	1
7	Queens gambit	2022-03-10 21:21:33	175.4	2
4	Womnder woman legend	2020-12-20 21:20:22	49.5	2
9	Fast and Furious 8	2021-08-07 21:20:22	99.5	1
\.


--
-- Data for Name: student; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.student (admission, name, age, course, department) FROM stdin;
3420	John	18	Computer Science                                  	ICT                                               
3419	Abel	17	Computer Science                                  	ICT                                               
3421	Joel	17	Computer Science                                  	ICT                                               
3422	Antony	19	Electrical Engineering                            	Engineering                                       
3423	Alice	18	Information Technology                            	ICT                                               
\.


--
-- Name: Movies_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Movies_id_seq"', 9, true);


--
-- Name: country_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.country_id_seq', 2, true);


--
-- Name: grades_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.grades_id_seq', 150, true);


--
-- Name: Employee Employee_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Employee"
    ADD CONSTRAINT "Employee_pkey" PRIMARY KEY ("Id");


--
-- Name: country country_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.country
    ADD CONSTRAINT country_pk PRIMARY KEY (id);


--
-- Name: grades grades_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grades
    ADD CONSTRAINT grades_pk PRIMARY KEY (id);


--
-- Name: movies movies_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movies
    ADD CONSTRAINT movies_pk PRIMARY KEY (id);


--
-- Name: student student_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.student
    ADD CONSTRAINT student_pkey PRIMARY KEY (admission);


--
-- Name: country_name_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX country_name_uindex ON public.country USING btree (name);


--
-- Name: movies_title_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX movies_title_uindex ON public.movies USING btree (title);


--
-- Name: movies movies_country_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movies
    ADD CONSTRAINT movies_country_id_fk FOREIGN KEY (country_id) REFERENCES public.country(id);


--
-- PostgreSQL database dump complete
--

