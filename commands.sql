------------------- upsert
create or replace function a_sp_upsert_movie(_title text, _release_date timestamp without time zone, _price double precision, _country_id bigint) returns bigint
    language plpgsql
as
$$
    DECLARE
        record_id bigint;
    BEGIN
        SELECT movies.id
        into record_id
            FROM movies
            WHERE movies.title = _title;
        If not found THEN
            INSERT INTO movies (title, release_date, price, country_id)
            VALUES (_title, _release_date, _price, _country_id)
            returning id into record_id;
        ELSE
            UPDATE movies
                SET release_date = _release_date, price = _price, country_id = _country_id
            WHERE
                movies.id = record_id;
            end if;
            return record_id;
    END;
$$;

-- 4,Womnder woman legend,2020-12-20 21:20:22.000000,49.5,2
select * from a_sp_upsert_movie('Womnder woman legend','2020-12-20 21:20:22.000000',49.5,2);
select * from a_sp_upsert_movie('Fast and Furious 8','2021-08-07 21:20:22.000000',99.5,1);

select * from movies;

create or replace function a_sp_div(x integer, y integer) returns double precision
    language plpgsql
as
$$
DECLARE
  result double precision := 0;
BEGIN
  if y = 0 THEN
      RAISE division_by_zero; -- this is like throw new error
  end if;
  result := x::double precision / y::double precision;
  return result;
EXCEPTION -- if we not catch the exception here -- it will be promoted to calling block
          -- raising errors and not catching them is like the SQL itself raised the exception
 WHEN division_by_zero THEN
 RAISE NOTICE 'caught division_by_zero'; -- this is message to console
 RETURN null; -- perhaps better, because we "swolloed" the error we need indicationg that the function failed

END;
$$;

select * from a_sp_div(4, 3);
select * from a_sp_div(4, 0);
